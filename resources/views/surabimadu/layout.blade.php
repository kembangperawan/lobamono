<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>surabimadu</title>
    {!!assets('bootstrap-3')!!}
    {!!assets('font-awesome')!!}
    {!!assets('roboto')!!}
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
        body {
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
            color: #333;
            padding-top: 100px;
        }
        .s-nav {
            border-bottom: 1px solid #DDD;
            padding: 20px 0;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            background: #FFF;
            z-index: 1001;
        }
        .s-brand {
            color: inherit;
            font-size: 1.4em;
            font-weight: bold;
            text-transform: uppercase;
        }
        .s-main-header {
            font-weight: bold;
            font-size: 1.2em;
            padding: 10px;
            border-bottom: 1px solid #DDD;
            margin-bottom: 20px;            
        }
        .s-item {
            margin-bottom: 20px;
            text-align: center;
        }
        .s-item .title {
            color: inherit;
            font-weight: bold;
        }
        .s-item img {
            width: 100%;
            margin-bottom: 10px;
        }
    </style>

    
  </head>
  <body>
    <nav class="s-nav">
        <div class="container text-center">
            <a class="s-brand" href="/">
                surabimadu
            </a>
        </div>
    </nav>
    <div class="container">
        <div class="row">
            <div class="s-main-header col-xs-12">
                Latest
            </div>
        </div>
        <div class="row">
            @for($i=0;$i<20;$i++)
            <div class="col-md-3 col-sm-6 col-xs-12 s-item">
                <a href="#">
                    <img src="{{URL('/assets/images/surabimadu/item.jpg')}}" alt="item" />
                </a>
                <a class="title">Image {{$i}}</a>
                <p>Some shit of description.</p>
            </div>
            @endfor
        </div>
    </div>
  </body>
</html>