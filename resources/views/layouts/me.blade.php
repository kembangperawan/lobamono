<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>feltclassic</title>

    <!-- Bootstrap -->
    <link href="{{URL('/assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL('/assets/css/me.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <style>
        #leftHalf {
            background:  url('{{URL("/assets/images/banner-new.jpg")}}') no-repeat; 
            background-size: cover;
            background-position: 50% 100%;
            width: 50%;
            position: fixed;
            left: 0px;
            height: 100%;
        }
        .navbar-brand{
            font-weight: 800;
            color: inherit;
        }
        @media (max-width: 768px) {
            .navbar-fixed-top,{
                top: 0;
                position: fixed;
                right: 0;
                left: 0;
                z-index: 1030;
            }
            body {
                padding-top: 51px;
            }
        }
        @media (min-width: 768px) {
            .navbar-fixed-top{
                position: static;
            }
        }
        
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div id="leftHalf" class="hidden-sm hidden-xs">
        <div class="me"><a href="/">emmards</a></div>
  </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-xs-12"></div>
            <div class="col-md-6 col-xs-12">
                <div class="top-nav navbar navbar-fixed-top">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand hidden-lg hidden-md" href="{{URL('/')}}">emmards</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav nav-pills nav-justified custom" style="margin-bottom: 20px;">
                            <li><a href="{{URL('/me')}}">Hi!</a></li>
                            <li><a href="{{URL('/blog')}}">Blog</a></li>
                            <li><a href="#">Portfolio</a></li>
                            <li><a href="#">Give me beer!</a></li>
                        </ul>
                    </div>
                </div>
                <!-- container -->
                @yield('content')
                <!-- end container -->
            </div>
        </div>
        
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{URL('/assets/js/bootstrap.min.js')}}"></script>
    </script>
  </body>
</html>