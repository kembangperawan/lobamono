<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>feltclassic</title>

    <!-- Bootstrap -->
    <link href="{{URL('/assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <style>
        body{
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
            color: #333;
        }
        a.logo, a.logo:hover {
            color: inherit;
            text-decoration: none;
        }
        a.cart {
            color: inherit;
        }
        .nav.custom {
            border: 1px solid #DDD;
            border-width: 1px 0;
        }
        .nav.custom a {
            color: inherit;
            text-transform: uppercase;
            font-weight: 300;
        }
        .nav.custom a:hover {
            background: transparent;
            text-decoration: underline;
        }
        .top-nav .icon-bar {
            background: #333;
        }
        .top-nav .navbar-toggle {
            background: transparent;
            border: 1px solid #DDD;
            margin-right: 0;
        }
        .top-nav .navbar-collapse {
            padding: 0;
        }
        .nopadding {
            margin: 0;
            padding: 0;
        }
        .products .col-md-4,
        .products .col-sm-6,
        .products .col-xs-12 {
            
        }
        .products a.item {
            display: block;
            color: inherit;
            text-decoration: none;
            margin-bottom: 20px;
        }
        .products a.item img {
            width: 100%;
        }
        .products a.item .bottom {
            padding: 8px 0;
            text-align: center;
            font-weight: 300;
        }
        .products a.item .bottom .title {
            text-transform: uppercase;
            font-size: 1.1em;
        }
        .products a.item .bottom .price {
            color: #777;
        }
        ul.footer-menu {
            padding: 0;
            margin: 0;
            list-style: none;
            text-transform: uppercase;
        }

        ul.footer-menu li {
            padding: 5px 0;
        }
        ul.footer-menu li a{
            text-decoration: none;
            color: inherit;
            font-size: 0.9em;
            font-weight: 300;
        }
        ul.footer-menu li a:hover {
            text-decoration: underline;
        }
        .footer {
            border-top: 1px solid #DDD;
            padding: 20px 0;
        }
        .social {
            padding: 10px 0;
        }
        .social a {
            text-decoration: none;
            color: inherit;
            font-size: 2em;
        }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 col-xs-12">
                <!-- logo container-->
                <div class="text-center" style="padding: 20px 0;">
                <span class="pull-right"><a href="{{URL('/')}}" class="cart"> <i class="fa fa-shopping-basket"></i> Your Cart(2)</a></span>
                    <h1><a href="{{URL('/')}}" class="logo">feltclassic</a></h1>
                </div>

                <!-- top navigation-->
                <div class="top-nav">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav nav-pills nav-justified custom" style="margin-bottom: 20px;">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Lookbook</a></li>
                            <li><a href="#">How to buy?</a></li>
                            <li><a href="#">Contact Us</a></li>
                        </ul>
                    </div>
                </div>

                <!-- container -->
                <div class="row products">
                    @for($i=0;$i<20;$i++)
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <a href="#" class="item">
                            <img src="https://cdn.shopify.com/s/files/1/0001/5211/products/momentplanner-large-1_900_475bc115-d4bf-47f9-8fd3-39503fb765e7_large.jpg?v=1498005353" alt="image" title="image" />
                            <div class="bottom">
                                <div class="title">Tas Bagus</div>
                                <div class="price">IDR 100.000</div>
                            </div>
                        </a>
                    </div>
                    @endfor
                </div>

                <!-- footer -->
                <div class="footer">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul class="footer-menu">
                                <li class="title">Info</li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">How to Buy?</a></li>
                                <li><a href="#">Shipping FAQ</a></li>
                                <li><a href="#">Return FAQ</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <ul class="footer-menu">
                                <li class="title">Tools</li>
                                <li><a href="#">Order Tracking</a></li>
                                <li><a href="#">Order Confirmation</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <h3>feltclassic</h3>
                            &copy;feltclassic 2017
                            <div class="social">
                                <a href="#"><i class="fa fa-instagram"></i></a>&nbsp;&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-facebook"></i></a>&nbsp;&nbsp;&nbsp;
                                <a href="#"><i class="fa fa-twitter"></i></a>&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{URL('/assets/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript">
        $('#bs-example-navbar-collapse-1').on('show.bs.collapse', function() {
        $('.nav-pills').addClass('nav-stacked');
    });

    //Unstack menu when not collapsed
    $('#bs-example-navbar-collapse-1').on('hide.bs.collapse', function() {
        $('.nav-pills').removeClass('nav-stacked');
    });
    </script>
  </body>
</html>