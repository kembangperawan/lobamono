@extends('layouts.me')
@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1 col-xs-12">
            <h1>Blog</h1>
            @for($i=0;$i<10;$i++)
            <div class="blog-item">
                <div class="blog-title">
                    <a href="{{URL('/blog/'.$i)}}">Blog Title {{$i}}</a>
                    <div class="pull-right text-muted date">17 Aug 17</div>    
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
            </div>
            @endfor
        </div>
    </div>
@stop