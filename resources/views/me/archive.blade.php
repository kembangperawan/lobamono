@extends('layouts.me')
@section('content')
    <div class="row">
        <div class="blog-detail col-md-10 col-md-offset-1 col-xs-12">
            <h1>Archive</h1>
            <ul class="list-unstyled">
            @for($i=0;$i<10;$i++)
                <li>{{$i + 1}} Dec 2017 - <a href="{{URL('/blog/'.$i)}}">Blog Post {{$i}}</a> </li>
            @endfor
            </ul>
            <hr />
            <p><a href="#"><i class="fa fa-arrow-up"></i> Bact to top</a></p>
        </div>
    </div>
@stop