@extends('layouts.me')
@section('content')
    <div class="row mobile-thumbnail hidden-md hidden-lg">
        <div class="col-xs-12">
            <p class="text-center"><img src="{{URL('/assets/images/thumbnail.jpg')}}" alt="thumbnail" width="200px" /></p>
            <p class="text-center social">
                <a href="https://www.instagram.com/emmards/" title="Instagram"><i class="fa fa-instagram"></i></a>&nbsp;&nbsp;
                <a href="https://twitter.com/emmards" title="Twitter"><i class="fa fa-twitter"></i></a>
            </p>
        </div>
    </div>
    <div class="row">
        <div style="padding: 125px 0;" class="hidden-xs hidden-sm"></div>
        <div class="col-md-6 col-md-offset-3 col-xs-12">
            <p><strong>Hi!</strong></p>
            <p class="greeting">
                I'm Emmanuel, from Bandung Indonesia.
            </p>
            <p class="greeting">
                I'm product manager, web designer and developer.
            </p>
        </div>
    </div>
@stop