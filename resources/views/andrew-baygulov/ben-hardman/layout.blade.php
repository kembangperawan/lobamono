<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>feltclassic</title>
    {!!css('bootstrap-3')!!}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <style>
        body{
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
            color: #333;
        }
        a.logo, a.logo:hover {
            color: inherit;
            text-decoration: none;
        }
        a.cart {
            color: inherit;
        }
        .nav.custom {
            border: 1px solid #DDD;
            border-width: 1px 0;
        }
        .nav.custom a {
            color: inherit;
            text-transform: uppercase;
            font-weight: 300;
        }
        .nav.custom a:hover {
            background: transparent;
            text-decoration: underline;
        }
        .top-nav .icon-bar {
            background: #333;
        }
        .top-nav .navbar-toggle {
            background: transparent;
            border: 1px solid #DDD;
            margin-right: 0;
        }
        .top-nav .navbar-collapse {
            padding: 0;
        }
        .nopadding {
            margin: 0;
            padding: 0;
        }
        .products .col-md-4,
        .products .col-sm-6,
        .products .col-xs-12 {
            
        }
        .products a.item {
            display: block;
            color: inherit;
            text-decoration: none;
            margin-bottom: 20px;
        }
        .products a.item img {
            width: 100%;
        }
        .products a.item .bottom {
            padding: 8px 0;
            text-align: center;
            font-weight: 300;
        }
        .products a.item .bottom .title {
            text-transform: uppercase;
            font-size: 1.1em;
        }
        .products a.item .bottom .price {
            color: #777;
        }
        ul.footer-menu {
            padding: 0;
            margin: 0;
            list-style: none;
            text-transform: uppercase;
        }

        ul.footer-menu li {
            padding: 5px 0;
        }
        ul.footer-menu li a{
            text-decoration: none;
            color: inherit;
            font-size: 0.9em;
            font-weight: 300;
        }
        ul.footer-menu li a:hover {
            text-decoration: underline;
        }
        .footer {
            border-top: 1px solid #DDD;
            padding: 20px 0;
        }
        .social {
            padding: 10px 0;
        }
        .social a {
            text-decoration: none;
            color: inherit;
            font-size: 2em;
        }
    </style>

    
  </head>
  <body>
  
  </body>
</html>