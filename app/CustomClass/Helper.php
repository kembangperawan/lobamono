<?php
    function assets($name) {
        $retval = config('assets.'.$name);
        return $retval != '' ? '<link rel="stylesheet" href="'.URL($retval).'">' : '';
    }
