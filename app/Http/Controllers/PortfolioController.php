<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PortfolioController extends Controller
{
    function index() {
        return view('me.index');
    }

    function blog() {
        return view('me.blog');
    }

    function get_blog($title) {
        return view('me.blog-detail');
    }

    function archive() {
        return view('me.archive');
    }

}
