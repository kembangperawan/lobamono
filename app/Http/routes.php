<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('layouts.front');
});

Route::resource('/design', 'WebDesignController');
Route::resource('/me', 'PortfolioController');
Route::get('/blog', 'PortfolioController@blog');
Route::get('/blog/{title}', 'PortfolioController@get_blog');
Route::get('/archive', 'PortfolioController@archive');

// Route::get('/design/{id}', 'WebDesignController@showContent');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
