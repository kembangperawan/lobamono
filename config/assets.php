<?php

return [

    // css
    'bootstrap-3' => '/assets/css/bootstrap.min.css',

    // font
    'roboto' => 'https://fonts.googleapis.com/css?family=Roboto:300,400,700',
    'font-awesome' => 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css'

];
